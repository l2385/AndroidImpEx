# AndroidImpEx
Import Export data from Android phones app, designed for Linux phones.

Tested with PINE64 PinePhone running Mobian.

![Screenshot](screenshots/default.png)

Open [Screenshots Gallery](./screenshots/README.md) for more images.

## Features
- Simple interface, select operation, execute, done!

- Exported files will be present in the home of the user
	/home/mobian 

- Application is able to import from Android

	- Contacts from:
/data/data/com.android.providers.contacts/databases/contacts2.db 

	- Call History from:
/data/data/com.sec.android.provider.logsprovider/databases/logs.db

	- Messages from:
/data/data/com.android.providers.telephony/databases/mmssms.db

- The 3 Android databases to be imported should be placed under: 
	- /home/mobian/AndroidDBs

- The 3 databases can be retrived from a connected (rooted) Android automatically
	- if this is run on a pc, Android needs to be connected via USB
	- if this is run on the Pinephone 
		Android needs to be connected via USB (or on-the-go)
		    - experimental at this stage
		    - make sure "adb devices" shows the Android
			    (internal modem may be listed as well)

- The 3 databases can be retrived from a connected Android manually, by 
using adb from a connected computer. Use first adb shell to copy them 
to /sdcard or ExtSdcard, then you can use adb pull from sdcard or usual 
share. Upload them to the correct folder on PinePhone afterwards.

- Application is able to import Contacts from SIM

- Data can replace or be merged with current data

- Application is automatically creating dated backups of modified files
    (in the same folder where the original file was located)

- Application is able to backup/restore at request


![Logo](androidimpex.png)

## Needs the following:

# Install dependencies on Mobian/Debian (if necessary):
sudo apt-get install libdbd-sqlite3-perl libgtk3-perl

# Install the desktop shortcut
- /usr/share/applications/androidimpex.desktop
file needs to belong to root.root and it shuld not be executable
file needs to point to the exact location of the perl script

# Install the icon in
- /usr/share/pixmaps/androidimpex.png

# Install the perl scripts in /usr/bin
- AndroidImpEx.pl

# restart phosh (if necessary)
- sudo systemctl restart phosh

# License

This software is licensed under the terms of the GNU General Public License,
version 3.

This project has never received any monetary contributions of any kind, from any party. It was all developed in my own 
free time and fully with my own resources. I consider it as my contribution/donation to the free software movement. 
Hope to be useful. All code included in this repository has been fully written by me.
