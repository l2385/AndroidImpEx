# App Screenshots
Functionality gallery of the Android database Import Export app.

## Home Screen
Simple initial screen: select the desired operation, execute, done!

![default](default.png)

## Partial operation status output presented in the second window 

![second_win](second_win.png)
![third_win](third_win.png)


