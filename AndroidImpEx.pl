#!/usr/bin/perl
#
# Copyright (C) Copyright 2022 lx b. <lxb1@yahoo.com>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version
# 2.1 of the License, or (at your option) version 3.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301  USA. See <http://www.gnu.org/licenses/> for details.

# needs perl DBI module; GTK3 module
# If it is not already installed, Debian-like systems  will allow:
# sudo apt-get install libdbd-sqlite3-perl libgtk3-perl

use strict;
use Gtk3 -init;
use Glib ('TRUE','FALSE');
use DBI;

my $selected_script,'Archive Current DBs to backup.tgz';
my $actionresult;
my $dt = localtime();
my $delete;
my $window;
my $filename;
my $home=$ENV{"HOME"};
my $AndroidDBs=$home."/AndroidDBs";
my $filename_prefix = "$home/contacts-importer/";
my $file;
my $inserted;
my $dbh;
my $db;
my $backup_filename;
my $gomver;
my $contacts_folder_ver;


#$dt=~s/ +|:/-/g;
$dt =~s/ +/\-/g;

show_main_win ();

if ($selected_script eq 'Migrate Contacts from Android Database') {
    if (!-f "$home/.local/share/evolution/addressbook/system/contacts.db") {
	display_error("Missing Contacts Database\n - verify gnome-contacts is installed");
	};
    Retrive_contacts_folder_Version();
    if ($contacts_folder_ver ne '12') {show_dbversion_issue();};
    if (!-f "$AndroidDBs/contacts2.db") { display_error("Missing Android Contacts Database\n - contacts2.db");};
    show_delete_merge();
    }

elsif ($selected_script eq 'Migrate Contacts From SIM') {
    if (!-f "$home/.local/share/evolution/addressbook/system/contacts.db") { 
	display_error("Missing Contacts Database\n - verify gnome-contacts is installed"); 
	};
    Retrive_contacts_folder_Version();
    if ($contacts_folder_ver ne '12') {show_dbversion_issue();};
    if (!-x "/usr/bin/mmcli") { display_error("Missing mmcli\n - cannot read from SIM");};
    show_delete_merge();
    }

elsif ($selected_script eq 'Migrate Call History from Android Database') {
    if (!-f "$AndroidDBs/logs.db") { display_error("Missing Android Call History Database\n - logs.db");};
    Retrive_gom_Version ();
    if ($gomver ne '2') {show_dbversion_issue();};
    show_delete_merge();
    }

elsif ($selected_script eq 'Migrate Messaging History from Android Database') {
    if (!-f "$home/.purple/chatty/db/chatty-history.db") { display_error("Missing Chatty Database\n - verify chatty is installed");};
    if (!-f "$AndroidDBs/mmssms.db") { display_error("Missing Android SMS Database\n - mmssms.db");};
    show_delete_merge();
    }

elsif ($selected_script eq 'Restore Current DBs from backup.tgz') {show_select_archive();}

elsif ($selected_script eq 'Retrive Android Databases via USB') {
    if (!-x "/usr/bin/adb") { display_error("Android Debug Bridge (/usr/bin/adb) Missing\n - verify it is installed");};

    my $devices = `/usr/bin/adb devices`;

    my @devices2 = split(/^/, $devices);
    my $devices3 = @devices2[1];
    my @devices4 = split(/\t/, $devices3);
    my $devices5 = @devices4[0];

#    print "devices5 is:\"".$devices5."\"\n";

    if ($devices5 !~ /[0-9a-f]{8}/) { 
        display_error("Android device cannot be detected\n - verify it is connected correctly");
        exit;
        };

    }

#print "Selected script before exec is:".$selected_script."\n";
#print "Delete before exec is:".$delete."\n";

execimpex();

#print "Execution finished, now we have to display the actionresult \n";

show_results_win();

#print"exiting\n" ;
exit ;

sub show_main_win () {

    $window = Gtk3::Window->new('toplevel');
    $window->set_title ('Android Import Export');
    $window->set_default_size(250,250);
    $window->set_border_width(20);
    $window->signal_connect('delete_event' => sub {Gtk3->main_quit()});

    # The list of buttons below belong to the same group
    # The Archive button is preselected until user makes a different selection
    # Function of each button is easily observable as label
    # All callbacks (except last one) go to the same routine, yet with a different label

    # create an image
    my $image = Gtk3::Image->new();
    $image->set_from_file('/usr/share/pixmaps/androidimpex.png');

    my $button1 = Gtk3::RadioButton->new();
    $button1->set_label('Migrate Contacts from Android Database');
    $button1->signal_connect('toggled' => \&toggled_cb);
    $button1->set_active(FALSE);

    my $button2 = Gtk3::RadioButton->new_from_widget($button1);
    $button2->set_label('Migrate Contacts From SIM');
    $button2->signal_connect('toggled' => \&toggled_cb);
    $button2->set_active(FALSE);

    my $button3 = Gtk3::RadioButton->new_from_widget($button1);
    $button3->set_label('Migrate Call History from Android Database');
    $button3->signal_connect('toggled' => \&toggled_cb);
    $button3->set_active(FALSE);

    my $button4 = Gtk3::RadioButton->new_from_widget($button1);
    $button4->set_label('Migrate Messaging History from Android Database');
    $button4->signal_connect('toggled' => \&toggled_cb);
    $button4->set_active(FALSE);

    my $button5 = Gtk3::RadioButton->new_from_widget($button1);
    $button5->set_label('Export Contacts as VCARDs (vcf)');
    $button5->signal_connect('toggled' => \&toggled_cb);
    $button5->set_active(FALSE);

    my $button6 = Gtk3::RadioButton->new_from_widget($button1);
    $button6->set_label('Export Call History as CSV');
    $button6->signal_connect('toggled' => \&toggled_cb);
    $button6->set_active(FALSE);

    my $button7 = Gtk3::RadioButton->new_from_widget($button1);
    $button7->set_label('Export Messaging History as CSV');
    $button7->signal_connect('toggled' => \&toggled_cb);
    $button7->set_active(FALSE);

    my $button8 = Gtk3::RadioButton->new_from_widget($button1);
    $button8->set_label('Archive Current DBs to backup.tgz');
    $button8->signal_connect('toggled' => \&toggled_cb);
    $button8->set_active(TRUE);

    my $button9 = Gtk3::RadioButton->new_from_widget($button1);
    $button9->set_label('Restore Current DBs from backup.tgz');
    $button9->signal_connect('toggled' => \&toggled_cb);
    $button9->set_active(FALSE);

    my $button10 = Gtk3::RadioButton->new_from_widget($button1);
    $button10->set_label('Retrive Android Databases via USB');
    $button10->signal_connect('toggled' => \&toggled_cb);
    $button10->set_active(FALSE);

    my $button11 = Gtk3::Button->new('Execute');
    $button11->set_label('Click to execute selected Import/Export operation');
    $button11->signal_connect('clicked',sub { Gtk3->main_quit; } );

    my $label_vertical = Gtk3::Label->new("  G\n  P\n  L\n  3\n   \n  ".pack("C*", 169)."\n  2\n  0\n  2\n  2\n   \n  l\n  x\n  b\n  1\n  @\n  y\n  a\n  h\n  o\n  o\n  .\n  c\n  o\n  m\n");

    # a grid to place the buttons
    my $grid = Gtk3::Grid->new();
    $grid->attach($image, 0, 0, 1, 1);
    $grid->attach($button1, 0, 1, 1, 1);
    $grid->attach($button2, 0, 2, 1, 1);
    $grid->attach($button3, 0, 3, 1, 1);
    $grid->attach($button4, 0, 4, 1, 1);
    $grid->attach($button5, 0, 5, 1, 1);
    $grid->attach($button6, 0, 6, 1, 1);
    $grid->attach($button7, 0, 7, 1, 1);
    $grid->attach($button8, 0, 8, 1, 1);
    $grid->attach($button9, 0, 9, 1, 1);
    $grid->attach($button10, 0, 10, 1, 1);
    $grid->attach($button11, 0, 11, 1, 1);
    $grid->attach_next_to($label_vertical, $image, 'right', 2, 12);


    # add the grid to the window
    $window->add($grid);

    # show the window and run the Application
    $window -> show_all();
    $selected_script='Archive Current DBs to backup.tgz';

    Gtk3->main();

}

sub toggled_cb {

    my ($button) = @_;

    my $state = 'unknown';
    my $button_label = $button->get_label();	

    # whenever the button is turned on, state is on
    if ($button->get_active()) {
	$state = 'on';
	$selected_script = $button_label;
    }
    # else state is off
    else {
	$state = 'off';
	$selected_script = '';
	}	
#    print ("$button_label was turned $state. The selected script is $selected_script \n");

}

sub display_error () {
    my ($message) = @_;
    my $dialog = Gtk3::Dialog->new();
    $dialog->set_title('Unrecoverable Error');
    $dialog->set_default_size(240,70);
    $dialog->set_border_width(20);
    $dialog->set_transient_for($window);
    $dialog->set_modal(1);
    $dialog->add_button('OK','ok');
    $dialog->signal_connect('response' => \&on_display_error_response);
    my $content_area = $dialog->get_content_area();
    my $label = Gtk3::Label->new($message);
    $content_area->add($label);
    $dialog->show_all(); 

    Gtk3->main();

    }

sub on_display_error_response {

    my ($widget, $response_id) = @_;
    $widget->destroy();
    Gtk3->main_quit;
    die;

    }

sub show_dbversion_issue {

    my $messagedialog = Gtk3::MessageDialog->new(	$window,
							'modal',
							'question',
							'ok_cancel',
							"The version of the Database is unsupported. \nClick OK to try anyway or Cancel to exit");

    $messagedialog->signal_connect('response'=>\&wrong_version_response);
    $messagedialog->set_title ('Wrong Version');
    $messagedialog->set_default_size(240,70);
    $messagedialog->set_border_width(20);
    $messagedialog->show();

Gtk3->main();

}

sub wrong_version_response {
    my ($widget, $response_id) = @_;
    if ($response_id eq 'ok') { 
        $actionresult=$actionresult."\n *** UNSUPPORTED VERSION *** \n";
    }
    else {
        die;
    }
    $widget->destroy();
    Gtk3->main_quit;
}

sub show_delete_merge {
    
    my $dialog = Gtk3::Dialog->new();
    $dialog->set_title('Delete or Merge');
    $dialog->set_transient_for($window);
    $dialog->set_modal(TRUE);

    $dialog->add_button('DELETE','1');
    $dialog->add_button('MERGE','2');

    $dialog->signal_connect('response' => \&dialog_response);

    my $content_area = $dialog->get_content_area();
    my $label = Gtk3::Label->new('Delete previous data or Merge it with the new one?');
    $content_area->add($label);

    $dialog->set_default_size(240,70);
    $dialog->set_border_width(20);
    $dialog->show_all();

    Gtk3->main();

}

sub dialog_response {
    
    my ($widget, $response_id) = @_;
    if ($response_id eq '1') {
	$delete=1;
#	print "deleting \n";
	}
    elsif (($response_id eq '2') or ($response_id eq 'delete-event')) {
	$delete=0;
#	print "merging \n";
	}
    $widget->destroy();
#    print $selected_script."\n";
#    print "delete is:".$delete."\n";
    Gtk3->main_quit;
}

sub show_select_archive {

    my $open_dialog = Gtk3::FileChooserDialog->new('Pick a file', 
						    $window,
						    'open',
						    ('gtk-cancel', 'cancel', 
						    'gtk-open', 'accept'));
    # not only local files can be selected in the file selector
    $open_dialog->set_local_only(TRUE);
    # dialog always on top of the textview window
    $open_dialog->set_modal(TRUE);

    # connect the dialog with the callback function open_response_cb()
    $open_dialog->signal_connect('response' => \&open_file_response);

    $open_dialog->set_title ('Select Archive to Restore?');
    $open_dialog->set_default_size(250,250);
    $open_dialog->set_border_width(20);

    $open_dialog->show();

    Gtk3->main();

}

sub open_file_response {

    my ($dialog, $response_id) = @_;
    my $open_dialog = $dialog;
    # if response id is 'ACCEPTED' (the button 'Open' has been clicked)
    if ($response_id eq 'accept') {
#	print "accept was clicked \n";

#	# $file is the file that we get from the FileChooserDialog
#	$file = $open_dialog->get_file();		

	$filename = $open_dialog->get_filename();
#	print "opened: $filename \n";

	$dialog->destroy();
	}
    # if response id is 'CANCEL' (the button 'Cancel' has been clicked)
    elsif ($response_id eq 'cancel') {
#	print "cancelled: Gtk3::FileChooserAction::OPEN \n";
	$dialog->destroy();
	}

#    print $selected_script."\n";
#    print "filename is:".$filename."\n";
    Gtk3->main_quit;
}


sub show_results_win {

    my $window3 = Gtk3::Window->new('toplevel');
    $window3->set_title('Executing selected Import/Export operation');
    $window3->set_default_size(400, 450);
    $window3->set_border_width(5);
    $window3->signal_connect('delete_event' => sub {Gtk3->main_quit()});

    my $vbox = Gtk3::VBox->new;

    # a scrollbar for the child widget (that is going to be the textview!)
    my $scrolled_window = Gtk3::ScrolledWindow->new();
    $scrolled_window->set_border_width(5);
    $scrolled_window->set_policy('automatic','automatic');

    my $buffer1 = Gtk3::TextBuffer->new();
    $buffer1->set_text("$actionresult", -1);

    my $textview = Gtk3::TextView->new();
    $textview->set_editable(FALSE);
    $textview->set_buffer($buffer1);
    $textview->set_wrap_mode('none');
    $scrolled_window->add($textview);

    # a button on the parent window
    my $closebutton = Gtk3::Button->new('Quit');
    $closebutton->signal_connect('clicked',sub { Gtk3->main_quit; } );
    #$closebutton->signal_connect('clicked'=>\&on_close);
    # here we should not close the whole application, yet it does not allow 
    # closing only of the last window for whatever reason

    $vbox->pack_start( $scrolled_window, 1, 1, 0 );
    $vbox->pack_start( $closebutton, 0, 0, 0 );

    $window3->add($vbox);

    $window3 -> show_all();
    Gtk3->main();

}


sub execimpex {

    if ($selected_script eq 'Migrate Contacts from Android Database')		{ImpCoA();}
    elsif ($selected_script eq 'Migrate Contacts From SIM')				{ImpCoS();}
    elsif ($selected_script eq 'Migrate Call History from Android Database')	{ImpCH();}
    elsif ($selected_script eq 'Migrate Messaging History from Android Database')	{ImpM();}
    elsif ($selected_script eq 'Export Contacts as VCARDs (vcf)')			{ExpCo();}
    elsif ($selected_script eq 'Export Call History as CSV')			{ExpCH();}
    elsif ($selected_script eq 'Export Messaging History as CSV')			{ExpM();}
    elsif ($selected_script eq 'Archive Current DBs to backup.tgz')			{Arch();}
    elsif ($selected_script eq 'Restore Current DBs from backup.tgz')		{Restore();}
    elsif ($selected_script eq 'Retrive Android Databases via USB')			{Android_USB_Retrieve();}
    else 							{ die "something went wrong";}
}


sub ImpCoA() {
    $actionresult="Choice 1 -Import Contacts from Android\n\n";

    my $database = "$home/.local/share/evolution/addressbook/system/contacts.db";
    OpenDB($database);

    # we are making changes - first create a backup of the existing database
    my $rows = $dbh->sqlite_backup_to_file($backup_filename);
    $actionresult=$actionresult."backup_filename is:\n".$backup_filename."\n\n";
    $actionresult=$actionresult.$rows;

    if ("$delete" eq "1") {
	DeleteContacts();
    };

    $dbh->do("attach database '$AndroidDBs/contacts2.db' as andro;");

    eval {
	$dbh->begin_work;

	$dbh->do("create temp view fullcontact as 
			select f.uid,
				Rev,
				file_as,
				file_as_localized,
				nickname,
				full_name,
				given_name,
				given_name_localized,
				family_name,
				family_name_localized,
				is_list,
				list_show_addresses,
				wants_html,
				x509Cert,
				pgpCert,
				vcard,
				bdata,
				e.uid,
				e.value as Email,
				p1.value as HomePhone,
				p2.value as CellPhone
			from folder_id as f
                        join folder_id_email_list as e on f.uid=e.uid 
                        left join folder_id_phone_list as p1 on f.uid=p1.uid 
                        left join folder_id_phone_list as p2 on f.uid=p2.uid and p1.value > p2.value
		");

	$dbh->do("create temp trigger insert_into_fullcontact_trigger
			instead of insert on fullcontact
			for each row
			    begin
	    insert or ignore into folder_id_email_list values((case when NEW.Email isnull then null else NEW.uid end),NEW.Email);
	    insert or ignore into folder_id_phone_list values((case when NEW.HomePhone isnull then null else NEW.uid end),NEW.HomePhone);
	    insert or ignore into folder_id_phone_list values((case when NEW.CellPhone isnull then null else NEW.uid end),NEW.CellPhone);
	    insert or ignore into folder_id values(
--					case when NEW.HomePhone == (select max(value) from folder_id_phone_list where value = NEW.HomePhone) 
--					      and NEW.CellPhone == (select max(value) from folder_id_phone_list where value = NEW.CellPhone)
--					    then null else NEW.uid end,
					NEW.uid,
					NEW.Rev,
					NEW.full_name,
					null,
					null,
					NEW.full_name,
					null,
					null,
					NEW.full_name,
					null,
					0,
					0,
					0,
					0,
					0,
					'\nBEGIN:VCARD\nVERSION:3.0\nUID:'||NEW.uid||'\nREV:'||NEW.Rev||'\nN:'||NEW.full_name||'\nFN:'||NEW.full_name||case when NEW.HomePhone isnull then '' else '\nTEL;HOME;VOICE:'||NEW.HomePhone end||case when NEW.CellPhone isnull then '' else '\nTEL;CELL:'||NEW.CellPhone end||'\nEND:VCARD\n',
					null);
		end;");

	$dbh->do("create temp view androfullcontact as 
		    select 
			(select 'pas-id-'||substr(u,1,32)||substr('00000000'||id, -8, 8) from (select lower(hex(randomblob(16))) as u)) as uid
			,strftime('%Y-%m-%dT%H:%M:%SZ',datetime('now')) as Rev
			,full_name as file_as
			,null as file_as_localized
			,null as nickname
			,full_name
			,null as given_name
			,null as given_name_localized
			,full_name as family_name
			,null as family_name_localized
			,0 as is_list
			,0 as list_show_addresses
			,0 as wants_html
			,0 as x509Cert
			,0 as pgpCert
			,null as vcard
			,null as bdata
			,null
			,null
			,CellPhone
			,HomePhone
			from (select DISTINCT
					d1.data1 as full_name
					,max(d1._id) as id
					,max(case when d2.data2==1 then d2.data1 else null end) as CellPhone
					,max(case when d3.data2==2 then d3.data1 else null end) as HomePhone
				    FROM andro.data as d1
				    join andro.data as d2 on d1.raw_contact_id=d2.raw_contact_id and d1.data2>9 and d2.data2<9 and d2.data1<9
				    join andro.data as d3 on d1.raw_contact_id=d3.raw_contact_id and d1.data2>9 and d2.data2<9 and d3.data2<9
					group by d1.data1);
		");

	$dbh->do("insert into fullcontact
		    select * from androfullcontact
		");

	$dbh->commit; 

	} or do {
	$actionresult=$actionresult."Database error: $DBI::errstr\n";
	$actionresult=$actionresult."rolling back\n";
	$dbh->rollback;
	};

    $dbh->disconnect();

}


sub ImpCoS() {
    $actionresult="Choice 2 -Import Contacts from SIMCard:\n\n";

    my $database = "$home/.local/share/evolution/addressbook/system/contacts.db";
    OpenDB($database);

    # first create a backup of the existing database
    my $rows = $dbh->sqlite_backup_to_file($backup_filename);
    $actionresult=$actionresult."backup_filename is:\n".$backup_filename."\n\n";
    $actionresult=$actionresult.$rows;


    if ("$delete" eq "1") {
	DeleteContacts();
    };


    eval {
	$dbh->do("create temp view fullcontact as 
			select f.uid,
				Rev,
				file_as,
				file_as_localized,
				nickname,
				full_name,
				given_name,
				given_name_localized,
				family_name,
				family_name_localized,
				is_list,
				list_show_addresses,
				wants_html,
				x509Cert,
				pgpCert,
				vcard,
				bdata,
				e.uid,
				e.value as Email,
				p1.value as HomePhone,
				p2.value as CellPhone
			from folder_id as f
                        join folder_id_email_list as e on f.uid=e.uid 
                        left join folder_id_phone_list as p1 on f.uid=p1.uid 
                        left join folder_id_phone_list as p2 on f.uid=p2.uid and p1.value > p2.value
		");

	$dbh->do("create temp trigger insert_into_fullcontact_trigger
			instead of insert on fullcontact
			for each row
			    begin
	    insert or ignore into folder_id_email_list values((case when NEW.Email isnull then null else NEW.uid end),NEW.Email);
	    insert or ignore into folder_id_phone_list values((case when NEW.HomePhone isnull then null else NEW.uid end),NEW.HomePhone);
	    insert or ignore into folder_id_phone_list values((case when NEW.CellPhone isnull then null else NEW.uid end),NEW.CellPhone);
	    insert or ignore into folder_id values(
--					case when NEW.HomePhone == (select max(value) from folder_id_phone_list where value = NEW.HomePhone) 
--					      and NEW.CellPhone == (select max(value) from folder_id_phone_list where value = NEW.CellPhone)
--					    then null else NEW.uid end,
					NEW.uid,
					NEW.Rev,
					NEW.full_name,
					null,
					null,
					NEW.full_name,
					null,
					null,
					NEW.full_name,
					null,
					0,
					0,
					0,
					0,
					0,
					'\nBEGIN:VCARD\nVERSION:3.0\nUID:'||NEW.uid||'\nREV:'||NEW.Rev||'\nN:'||NEW.full_name||'\nFN:'||NEW.full_name||case when NEW.HomePhone isnull then '' else '\nTEL;HOME;VOICE:'||NEW.HomePhone end||case when NEW.CellPhone isnull then '' else '\nTEL;CELL:'||NEW.CellPhone end||'\nEND:VCARD\n',
					null);
		end;");

	} or do {
	$actionresult=$actionresult."Database error creating views:\n $DBI::errstr\n";
	};

    # then retrive contacts from SIM Card and extract data 
    my $modemresult=`mmcli -m any --command='AT+CPBR=1,100'`;
    #my $modemresult=`cat SIMList.txt`;
    $modemresult=~s/.*: [0-9]*,//g;
    $modemresult=~s/![a-zA-Z,\"]*//g;
    $modemresult=~s/\'$/\"/g;

    my $row='';
    my $rowcount=0;

    my $full_name; 
    my $type;
    my $number;
    my $CellPhone;
    my $HomePhone;

    my @rows = split(/^/, $modemresult);

    foreach $row(@rows) {

	$row=~ s/\"//g;
	$row=~ s/\n//g;
	($number,$type,$full_name)=split(/,/,$row);
	$actionresult=$actionresult."Importing:".$full_name.",".$type.",".$number."\n";
	$rowcount++;

#	if ($type eq 128) {$HomePhone=$number;$CellPhone=null}
#	else {$CellPhone=$number;$HomePhone=null}

    eval {
	$dbh->begin_work;

#	$dbh->do("insert into fullcontact values (1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,1);");


	$dbh->do("insert into fullcontact values (
			(select 'pas-id-'||substr(u,1,40) from (select lower(hex(randomblob(16))) as u))
			,strftime('%Y-%m-%dT%H:%M:%SZ',datetime('now'))
			,\"$full_name\"
			,null
			,null
			,\"$full_name\"
			,null
			,null
			,\"$full_name\"
			,null
			,0
			,0
			,0
			,0
			,0
			,null
			,null
			,null
			,null
			,case when $type==128 then null else \"$number\" end
			,case when $type==128 then \"$number\" else null end
		);
		");

	$dbh->commit;

	} or do {
	$actionresult=$actionresult."Database error: $DBI::errstr\n";
	$actionresult=$actionresult."rolling back\n";
	$dbh->rollback;
	};


	}


    $dbh->disconnect();

}


sub ImpCH() {
    $actionresult="choice 3 -Import Call History\n\n";

    my $database = "$home/.local/share/calls/records.db";
    OpenDB($database);

    # first create a backup of the existing database
    my $rows = $dbh->sqlite_backup_to_file($backup_filename);
    $actionresult=$actionresult."backup_filename is:\n".$backup_filename."\n\n";
    $actionresult=$actionresult.$rows;

    if ("$delete" eq "1") {
    # delete current contents of the calls database
	$actionresult=$actionresult."deleting calls database\n";

        eval {

	    $dbh->begin_work;
	    $dbh->do("Delete from calls;");
	    $dbh->commit;

	} or do {

	    $actionresult=$actionresult."Database error: $DBI::errstr\n";
	    $actionresult=$actionresult."rolling back\n";
	    $dbh->rollback;
	};

    };

    # then retrieve from Android database
    $dbh->do("attach database '$AndroidDBs/logs.db' as andro;");
    $dbh->do("attach database ':memory:' as mem;");

    eval {
	$dbh->begin_work;

	$dbh->do("create table mem.calls as select * from calls;");
	$dbh->do("insert into mem.calls select andro.logs._id as id,
						number as target,
						type-1 as inbound,
						strftime('%Y-%m-%dT%H:%M:%S.00000Z',ROUND(date/1000),'unixepoch') as start,
						strftime('%Y-%m-%dT%H:%M:%S.000000Z',ROUND(date/1000),'unixepoch') as answered,
						strftime('%Y-%m-%dT%H:%M:%S.000000Z',duration + ROUND(date/1000),'unixepoch') as end,
						'tel' as protocol 
				from andro.logs where logtype=100 and type <3;");
	$dbh->do("update mem.calls set id=null;");

	$dbh->do("update sqlite_sequence SET seq = 0 WHERE name = 'calls';");
	$dbh->do("delete from calls;");
#	$dbh->do("delete from calls3;");
	$dbh->do("insert into calls select * from mem.calls order by start;");

	$dbh->commit;
    } or do {
	$actionresult=$actionresult."Database error: $DBI::errstr\n";
	$actionresult=$actionresult."rolling back\n";
	$dbh->rollback;
    };

    $dbh->disconnect();
}


sub ImpM() {
    $actionresult="choice 4 -Import Messages\n No further output expected\n\n";

    my $database = "$home/.purple/chatty/db/chatty-history.db";
    OpenDB($database);

    # first create a backup of the existing database
    my $rows = $dbh->sqlite_backup_to_file($backup_filename);
    $actionresult=$actionresult."backup_filename is:\n".$backup_filename."\n\n";
    $actionresult=$actionresult.$rows;

    # Delete Messages if requested
    if ("$delete" eq "1") {
	DeleteMessages();
    };

    $dbh->do("attach database '$AndroidDBs/mmssms.db' as andro;");

    # here we perform the actual importing
    eval {
	$dbh->begin_work;

	$dbh->do("create temp view fullmessage as 
			select * from messages as m 
			left join users as u on m.sender_id=u.id 
			left join threads as t on m.thread_id=t.id 
			left join thread_members as tm on t.id=tm.id;");
	$dbh->do("create temp trigger insert_into_fullmessage_trigger
			instead of insert on fullmessage
			for each row
			    begin
	    insert or ignore into users values(null,NEW.sender_id,null,null,1);
	    insert or ignore into threads values(null,NEW.sender_id,NEW.sender_id,null,2,0,0,null,0);
	    insert or ignore into thread_members values(null,(select max(id) from threads where name = NEW.sender_id),
							(select max(id) from users where username = NEW.sender_id));
	    insert into messages values (NEW.id,
					(select substr(u,1,8)||'-'||substr(u,9,4)||'-4'||substr(u,13,3)||'-'||v||substr(u,17,3)||'-'||substr(u,21,12)
						from (select lower(hex(randomblob(16))) as u, substr('89ab',abs(random()) % 4 + 1, 1) as v)),
					(select max(id) from threads where name = NEW.sender_id),
					(select max(id) from users where username = NEW.sender_id),
					NEW.user_alias,
					NEW.body,
					NEW.body_type,
					NEW.direction,
					NEW.time,
					NEW.status,
					NEW.encrypted,
					NEW.preview_id);
			    end;");
	$dbh->do("insert into fullmessage select null as id,address as uid,(select max(id) from threads where name = address) as thread_id,address as sender_id,person as user_alias,body,1 as body_type,(2*type)-3 as direction,date/1000 as time,null as status, 0 as encrypted, null as preview_id,null,address,null,null,1,null,address,address,null,2,0,0,null,0,null,(select max(id) from threads where name = address),(select max(id) from users where username = address) FROM andro.sms;");

	$dbh->commit; 

	} or do {
	$actionresult=$actionresult."Database error: $DBI::errstr\n";
	$actionresult=$actionresult."rolling back\n";
	$dbh->rollback;
	};

    $dbh->disconnect();

}

sub ExpCo() {
    $actionresult="choice 5 -Expport Contacts\n\n";

    my $filename = "$home/Export_Contacts.vcf";
    open(FH, '>', $filename) or die $!;
    #print("File $filename opened successfully!\n");

    my $database = "$home/.local/share/evolution/addressbook/system/contacts.db";
    OpenDB($database);

    my $stmt = qq(
    select vcard from folder_id where file_as not null;
    );

    my $rows = $dbh->selectall_arrayref($stmt);
    my $row='';
    my $row2='';
    my $column='';

    foreach $row (@$rows) {
	foreach $column (@$row) {

	    $row2='';

	    $column=~s/.*BEGIN:VCARD/BEGIN:VCARD/;
	    $column=~s/END:VCARD.*/END:VCARD/;
	    $column=~s/UID:.*\n//;
	    $column=~s/REV:.*\n//;

	    $row2=$row2.",".$column;
	    print FH $column."\n";
	    $actionresult=$actionresult.$column."\n";

	}
	print FH "\n";
	$actionresult=$actionresult."\n";
    }

    close(FH);
    $dbh->disconnect();

}

sub ExpCH() {
    $actionresult="choice 6 -Export Call History\n\n";

    my $filename = "$home/Export_CallHistory.csv";
    open(FH, '>', $filename) or die $!;
    #print("File $filename opened successfully!\n");

    my $database = "$home/.local/share/calls/records.db";
    OpenDB($database);

    my $stmt = qq(
    select * from calls order by start;
    );

    my $rows = $dbh->selectall_arrayref($stmt);
    my $row='';
    my $row2='';
    my $column='';

    foreach $row (@$rows) {
	$row2='';
	foreach $column (@$row) {
	    $row2=$row2.$column.",";
	    }
	$row2=~s/,$//;
	print FH $row2."\n";
	$actionresult=$actionresult.$row2."\n";
	}

    close(FH);
    $dbh->disconnect();
}

sub ExpM() {
    $actionresult="choice 7 -Export Messages\n\n";

    my $filename = "$home/Export_Messages.csv";
    open(FH, '>', $filename) or die $!;
    #print("File $filename opened successfully!\n");

    my $database = "$home/.purple/chatty/db/chatty-history.db";
    OpenDB($database);

    my $stmt = qq(
    select * from messages as m 
		left join users as u on m.sender_id=u.id 
		left join threads as t on m.thread_id=t.id 
		left join thread_members as tm on t.id=tm.id;
    );

    my $rows = $dbh->selectall_arrayref($stmt);
    my $row='';
    my $row2='';
    my $column='';

    foreach $row (@$rows) {
	$row2='';
	foreach $column (@$row) {
	    $row2=$row2.$column.",";
	    }
	$row2=~s/,$//;
	print FH $row2."\n";
	$actionresult=$actionresult.$row2."\n";
	}

    close(FH);
    $dbh->disconnect();
}

sub Arch() {
    $actionresult="choice 8 - Archive\n\n";

    $actionresult=$actionresult.`tar cfvz $home/Contacts_Chat_and_Call_history-Export-$dt.tgz $home/.purple $home/.mms $home/.local/share/calls $home/.local/share/evolution/addressbook 2>&1`;
    #$actionresult="printing tar results";
}

sub Restore() {
    $actionresult="choice 9 - Restore\n\n";

    # this one can only restore the same second ... --)
    $actionresult=$actionresult.`tar xfvz $filename -C / 2>&1`;
    #$actionresult="printing tar results";
}


sub Android_USB_Retrieve {
    $actionresult="choice 10 - Retrieve Android Databases via USB\n\n";

    # create folder if it does not exist
    if (!-d $AndroidDBs) {mkdir($AndroidDBs);}; 

    $actionresult=$actionresult.`adb shell "su -c \"cp /data/data/com.android.providers.contacts/databases/contacts2.db /mnt/sdcard/\"" 2>&1`;
    $actionresult=$actionresult.`adb shell "su -c \"cp  /data/data/com.sec.android.provider.logsprovider/databases/logs.db /mnt/sdcard/\"" 2>&1`;
    $actionresult=$actionresult.`adb shell "su -c \"cp  /data/data/com.android.providers.telephony/databases/mmssms.db /mnt/sdcard/\"" 2>&1`;

    $actionresult=$actionresult.`cd $AndroidDBs/; adb pull /mnt/sdcard/contacts2.db 2>&1`;
    $actionresult=$actionresult.`cd $AndroidDBs/; adb pull /mnt/sdcard/logs.db 2>&1`;
    $actionresult=$actionresult.`cd $AndroidDBs/; adb pull /mnt/sdcard/mmssms.db 2>&1`;

    $actionresult=$actionresult.`adb shell "su -c \"rm /mnt/sdcard/contacts2.db\"" 2>&1`;
    $actionresult=$actionresult.`adb shell "su -c \"rm /mnt/sdcard/logs.db\"" 2>&1`;
    $actionresult=$actionresult.`adb shell "su -c \"rm /mnt/sdcard/mmssms.db\"" 2>&1`;
}


sub Retrive_gom_Version() {

    my $database = "$home/.local/share/calls/records.db";
    OpenDB($database);

    my $stmt = ("select max (version) from _gom_version;");

    my $rows = $dbh->selectall_arrayref($stmt);

    my $row = @$rows[0];
    $gomver = @$row[0];

#    print "gomver is:".$gomver;

$dbh->disconnect();
}



sub Retrive_contacts_folder_Version() {

    my $database = "$home/.local/share/evolution/addressbook/system/contacts.db";
    OpenDB($database);

    my $stmt = ("select max(version) from folders;");

    my $rows = $dbh->selectall_arrayref($stmt);

    my $row = @$rows[0];
    $contacts_folder_ver = @$row[0];

#    print "gomver is:".$gomver;

$dbh->disconnect();
}


sub OpenDB($db) {
# first create a backup of the existing database
# then open the Android contacts2 database and extract data 
    my $database = @_[0];
    #print "Current Database is:".$database."\n";

    $backup_filename = $database;
    $backup_filename =~ s/\.db/-$dt\.db/;

    my $driver   = "SQLite";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $password = "";
    #$dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 })
    $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1, PrintError => 1, AutoCommit => 1, HandleError => \&handle_error})
		or die $DBI::errstr;
    #print "Opened database successfully\n";
    $actionresult = $actionresult."Opened database successfully\n";

}


sub handle_error {

    $dbh->rollback; 
    my $error = shift;

    $actionresult = $actionresult."An error occurred in the script\n";
    $actionresult = $actionresult."Message: $error\n";

#    print $actionresult;

    return 1;
}


sub DeleteContacts() {
    $actionresult=$actionresult."deleting contacts database\n";
    my $rows;

    eval {
	$dbh->begin_work;

#	$dbh->do("Delete from folders;");
#	$dbh->do("Delete from keys;");
	$dbh->do("Delete from folder_id;");
	$dbh->do("Delete from folder_id_email_list;");
	$dbh->do("Delete from folder_id_phone_list;");

	$dbh->commit;
	} or do {

	    $actionresult=$actionresult."Database error: $DBI::errstr\n";
	    $actionresult=$actionresult."rolling back\n";
	    $dbh->rollback;
	};

}

sub DeleteMessages() {
    $actionresult=$actionresult."deleting messagess database\n";
    my $rows;

    eval {
	$dbh->begin_work;

	$dbh->do("delete from messages;");
	$dbh->do("delete from threads;");
	$dbh->do("delete from thread_members;");
	$dbh->do("delete from users where username <> 'SMS' and username <> 'MMS';");
	$dbh->do("delete from accounts;");
	$dbh->do("delete from mime_type;");
	$dbh->do("delete from files;");
	$dbh->do("delete from video;");
	$dbh->do("delete from image;");
	$dbh->do("delete from audio;");


	$dbh->commit;
	} or do {

	    $actionresult=$actionresult."Database error: $DBI::errstr\n";
	    $actionresult=$actionresult."rolling back\n";
	    $dbh->rollback;
	};

}

sub clean_contacts_importer {
opendir my $dir, $filename_prefix or die "Cannot open directory: $!";
    my @files = readdir $dir;

    foreach  $file (@files) { 
#    print $filename_prefix.$file."\n";
    unlink $filename_prefix.$file;
    }
closedir $dir;
}
